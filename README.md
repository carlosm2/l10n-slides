
## ECSL 2020 - Hagamos mas accesible el software libre a nuestras comunidades

![essl2020](https://cacu.tech/img/ecsl2020.png)

**Descripción**: Colaborar en la traducción (localización) de software libre es una manera de aprender sobre el software que usamos, la comunidad que lo desarrolla y a la vez podemos acercar a mas personas información relevante.

Slides/Diapositivas: https://cacu.tech/ecsl2020/

Evento: Encuentro Centroamericano de Software Libre 2020

https://softwarelibre.ca/

